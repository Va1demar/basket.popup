basketPopup = {
    basket: {},

    PIDs: [],

    BASKET_STRING: null,

    fullMeasure: false,

    init: () => {
        basketPopup.fullMeasure = $('.p-cart-product__count-measure').length > 0 ? true : false;
        let pCart = $('.p-cart');
        if (pCart.is("[data-products]")) {
            basketPopup.PIDs = pCart.data('products').split(',').filter(function(e){return e});
            basketPopup.getBasket(basketPopup.PIDs);
        } else if (pCart.is("[data-samples]")) {
            basketPopup.PIDs = pCart.data('samples').split(',').filter(function(e){return e});
            if (basketPopup.PIDs.length > 0) {
                basketPopup.getSamplePrice(basketPopup.PIDs);
            }
        } else {
            return;
        }
    },

    getBasket: (arPids) => {
        BX.ajax.runComponentAction('custom:basket.popup',
        'getBasket', {
            mode: 'class',
            data: {pids: arPids}
        })
        .then(function(response) {
            if (response.status === 'success') {
                let result = response.data;
                basketPopup.setBasketPrice(result.ALL_COUNT, result.ALL_SUMM);
                basketPopup.setPriceItem(result.ITEM.RESULT_PRICE);
                basketPopup.setValueQuantity(result.ITEM.QUANTITY);
                basketPopup.BASKET_STRING = result.BASKET_STRING;
            }
        });
    },

    setQuantityBasket: (quantity) => {
        if (basketPopup.BASKET_STRING) {
            BX.ajax.runComponentAction('custom:basket.popup',
            'setQuantity', {
                mode: 'class',
                data: {
                    stringIDs: basketPopup.BASKET_STRING,
                    quantityData: quantity
                }
            })
            .then(function(response) {
                if (response.status === 'success') {
                    basketPopup.getBasket(basketPopup.PIDs);
                }
            });
        }
    },

    setPriceItem: (objPrice) => {
        let priceBlock = $('.p-cart-product__price'),
            curPriceBlock = priceBlock.find('.p-cart-product__price-current'),
            oldPriceBlockWrap = priceBlock.find('.p-cart-product__price-old-block'),
            oldPriceBlock = oldPriceBlockWrap.find('.p-cart-product__price-old-price'),
            percentPriceBlock = oldPriceBlockWrap.find('.p-cart-product__price-old-percent'),
            creditPriceBlock = priceBlock.find('.p-cart-product__price-credit-price');
        curPriceBlock.html(basketPopup.formatPrice(objPrice.PRICE));
        if (Number(objPrice.DISCOUNT)) {
            oldPriceBlockWrap.css({display: "flex"});
            oldPriceBlock.html(basketPopup.formatPrice(objPrice.BASE_PRICE));
            percentPriceBlock.html(basketPopup.formatPercent(objPrice.DISCOUNT_PERCENT));
        }

        if (creditPriceBlock) {
            let creditPrice =  Math.ceil(Number(objPrice.PRICE) / 3);
            creditPriceBlock.html(basketPopup.formatPrice(creditPrice));
        }

        priceBlock.css({opacity: 1});
    },

    setBasketPrice: (count, price) => {
        let blockBasket = $('.p-cart-bottom__info-basket'),
            stringResult = `В корзине ${count} ${basketPopup.declOfNum(count, ['товар', 'товара', 'товаров'])} на сумму ${basketPopup.formatPrice(price)}`;
        blockBasket.html(stringResult);
        blockBasket.css({opacity: 1,});
        return false;
    },

    setValueQuantity: (value) => {
        value = Number(value);
        let input = $('.p-cart-product__count-input');
        input.data("value", value);
        if (basketPopup.fullMeasure){
            basketPopup.setValueAll(value, input);
        } else {
            input.val(basketPopup.formatQuantity(value));
            basketPopup.showTogglePriceOne(value);
        }
    },

    setValueAll: (value, inputBlock) => {
        let kvmBtn = $(`.p-cart__measure-item[data-measure="m2"]`),
            upakBtn = $(`.p-cart__measure-item[data-measure="upak"]`),
            shtukBtn = $(`.p-cart__measure-item[data-measure="shtuk"]`),
            actMeasure = $(`.p-cart__measure-item.act`).data('measure');

        kvmBtn.data("value", value);

        let korobkiCount = basketPopup.shortNumber(Number(value) / Number(kvmBtn.data("count")));
        upakBtn.data("value", korobkiCount);

        let shtukCount =  korobkiCount * Number(shtukBtn.data("count"));
        shtukBtn.data("value", shtukCount);

        switch (actMeasure) {
            case 'upak':
                inputBlock.val(basketPopup.formatQuantity(korobkiCount));
                break;
            case 'shtuk':
                inputBlock.val(basketPopup.formatQuantity(shtukCount));
                break;
            default:
                inputBlock.val(basketPopup.formatQuantity(value));
                break;
        }
    },

    setBasketString: (basketString) => {
        $('.p-cart').data('id-basket', basketString);
    },

    getSamplePrice: (arPids) => {
        BX.ajax.runComponentAction('custom:basket.popup',
        'getSamplePrice', {
            mode: 'class',
            data: {pids: arPids}
        })
        .then(function(response) {
            if (response.status === 'success') {
                let result = response.data;
                basketPopup.setPriceItem(result.ITEM.RESULT_PRICE);
                basketPopup.BASKET_STRING = result.BASKET_STRING;
                basketPopup.setSamples();
            }
        });
    },

    setSamples: () => {
        BX.ajax.runComponentAction('custom:basket.popup',
        'getSamples', {
            mode: 'class',
            data: {}
        })
        .then(function(response) {
            if (response.status === 'success') {
                basketPopup.setSamplesCount(response.data.length);
            }
        });
    },

    setSamplesCount: (count) => {
        let blockBasket = $('.p-cart-bottom__info-basket'),
            stringResult = `Образцов в корзине: <span class="count">${count}</span>/10`;
        blockBasket.html(stringResult);
        blockBasket.css({opacity: 1,});
        return false;
    },

    clickUnit: (type) => {
        let input = $('.p-cart-product__count-input');
            value = Number(input.data('value')),
            count = Number(input.data('count')),
            newValue = value + count;
        if (type == "minus") newValue = value - count;
        if (newValue >= count) {
            input.val(basketPopup.formatQuantity(newValue));
            input.data('value', newValue);
            basketPopup.setQuantityBasket(newValue);
        }
    },

    clickUnitPlitka: (type) => {
        let input = $('.p-cart-product__count-input');
        $('.p-cart__measure-item').each(function (index, val) {
            let element = $(val),
                value = Number(element.data('value')),
                count = Number(element.data('count')),
                newValue = value + count;
            if (type == "minus") newValue = value - count;
            newValue = basketPopup.shortNumber(newValue);
            if (newValue >= count) {
                element.data('value', newValue);
                if (element.hasClass('act')) {
                    input.val(basketPopup.formatQuantity(newValue));
                }
                if (element.data('measure') == 'm2') {
                    input.data('value', newValue);
                    basketPopup.setQuantityBasket(newValue);
                }
            }
        });
    },

    enterQuantityPlitka: (valueInput) => {
        let actMeasureBlock = $('.p-cart__measure-item.act');
            activeMeasure = actMeasureBlock.data('measure'),
            count = actMeasureBlock.data('count'),
            boxCount = basketPopup.getBox(count, valueInput);
        let inputBlock = $('.p-cart-product__count-input');
        $('.p-cart__measure-item').each(function (index, val) {
            let $this = $(val),
                countItem = Number($this.data('count')),
                measure = $this.data('measure'),
                result = basketPopup.shortNumber(countItem * boxCount);
            $this.data('value', result)
            if (measure == 'm2'){
                inputBlock.data('value', result);
                basketPopup.setQuantityBasket(result);
            }
            if ($this.hasClass('act')) {
                inputBlock.val(basketPopup.formatQuantity(result));
            }
        });
    },

    enterQuantity: (valueInput) => {
        let newValue = Math.ceil(basketPopup.transformNumber(valueInput)),
            inputBlock = $('.p-cart-product__count-input');
        inputBlock.val(newValue);
        inputBlock.data('value', newValue);
        basketPopup.setQuantityBasket(newValue);
    },

    getBox: (count, value) => {
        return Math.ceil(value / count);
    },

    shortNumber: (value) => {
        if (!Number.isInteger(value)) {
            return Number(value.toFixed(3).replace(/0*$/,""));
        }
        return value;
    },

    transformNumber: (value) => {
        let result = Number(String(value).replace(',', '.'));
        if (!isNaN(result) && result > 0) {
            return result;
        }
        return 1;
    },

    checkInputQuantity: (value) => {
        return value.replace(/[^(.|,)\d]+/g,"").replace( /^([^\.]*\.)|\./g, '$1' ).replace( /^([^\,]*\,)|\,/g, '$1' );
    },

    formatQuantity: (value) => {
        return String(value).replace('.', ',');
    },

    formatPrice: (price) => {
        let decimal = 0;
            separator = ' ';
            decpoint = '.';
            format_string = '# ₽';
        let r = parseFloat(price);
        let exp10 = Math.pow(10,decimal);
        r = Math.round(r*exp10)/exp10;
        rr = Number(r).toFixed(decimal).toString().split('.');
        b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);
        r = (rr[1]?b+ decpoint +rr[1]:b);
        return format_string.replace('#', r);
    },

    formatPercent: (percent) => {
        return '-' + percent + '%';
    },

    declOfNum: (number, titles) => {  
        cases = [2, 0, 1, 1, 1, 2];  
        return titles[(number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5]];  
    },

    showTogglePriceOne: (value) => {
    let blockPriceOne = $('.p-cart-product__count-price');
        if (value > 1) {
            blockPriceOne.removeClass('invisible');
        } else {
            blockPriceOne.addClass('invisible');
        }
    },

    makeDelay: (ms = 500) => {
        var timer = 0;
        return function(callback){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    },

    removePopup: () => {
        basketPopup.PIDs = [];
        basketPopup.BASKET_STRING = null;
        $('.p-cart__back').remove();
    }
}

//удаляем попап
$(document).on('click', '.p-cart__back', function(event){
    if ($(event.target).hasClass('p-cart__back')) {
        basketPopup.removePopup();
    }
});
$(document).on('click', '.p-cart__cross, #continue_buy', function(){
    basketPopup.removePopup();
});

//переключение единиц измерения
$(document).on('click', '.p-cart__measure-item:not(.act)', function(){
    $('.p-cart__measure-item').removeClass('act');
    let $this = $(this),
        price = $this.data('price'),
        unit = $this.data('unit'),
        value = $this.data('value');
    $this.addClass('act');
    let stringPrice = basketPopup.formatPrice(price) + '/' + unit,
        letStringUnit = unit;
    if (unit == 'м'){
        stringPrice += '<sup>2</sup>';
        letStringUnit += '<sup>2</sup>';
    }
    $('.p-cart-product__count-price').html(stringPrice);
    $('.p-cart__measure-name').html(letStringUnit);
    $('.p-cart-product__count-input').val(basketPopup.formatQuantity(value));
});

//кнопка минус
$(document).on('click', '.p-cart-product__count-sign.minus', function(){
    if (basketPopup.fullMeasure) {
        basketPopup.clickUnitPlitka('minus');
    } else {
        basketPopup.clickUnit('minus');
    }
});

//кнопка плюс
$(document).on('click', '.p-cart-product__count-sign.plus', function(){
    if (basketPopup.fullMeasure) {
        basketPopup.clickUnitPlitka('plus');
    } else {
        basketPopup.clickUnit('plus');
    }
});

//проверка инпут на число
$(document).on('keyup', '.p-cart-product__count-input', function(){
    this.value = basketPopup.checkInputQuantity(this.value);
});

//ввод кол-ва в инпут
$(document).on('change', '.p-cart-product__count-input', function(){
    if (basketPopup.fullMeasure) {
        let value = this.value;
        basketPopup.enterQuantityPlitka(value);
    }
});

$(document).on('keyup', '.p-cart-product__count-input', function(){
    if (!basketPopup.fullMeasure) {
        let value = this.value;
        basketPopup.makeDelay(700)(basketPopup.enterQuantity(value));
    }
});

//10% на подрезку
$(document).on('change', '#ten-percent', function(){
    let value = basketPopup.transformNumber($('.p-cart-product__count-input').val()),
        checkbox = $(this);
    value = checkbox.is(':checked') ? value * 1.1 : value * 0.9;
    if (basketPopup.fullMeasure) {
        basketPopup.enterQuantityPlitka(value);
    } else {
        basketPopup.enterQuantity(value);
    }
});

//слайдер наборов
$(document).on('click', '.p-cart-set__title', function(){
    $(this).toggleClass('visible');
    $('.p-cart-set__content').slideToggle();
});

if(window.innerWidth < 1000){
    //список ед.измерения в мобилке
    $(document).on('click', '.p-cart__measure-name, .p-cart__measure-item', function(){
        $('.p-cart__measure-name').toggleClass('visible');
    });
}