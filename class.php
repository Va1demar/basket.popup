<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
	Bitrix\Main\Loader,
	Bitrix\Main\Error,
	Bitrix\Main\ErrorCollection,
	Bitrix\Main\SystemException,
	Bitrix\Sale,
	Bitrix\Sale\Basket,
	Bitrix\Sale\Fuser,
	Bitrix\Sale\DiscountCouponsManager,
	Bitrix\Sale\PriceMaths,
	Bitrix\Iblock,
	Bitrix\Main\Page\Asset,
	Bitrix\Main\Engine\Contract\Controllerable,
	Bitrix\Catalog,
	Bitrix\Main\Grid\Declension;

Loader::includeModule("sale");
Loader::includeModule("catalog");

class CCustomBasketPopup extends CBitrixComponent implements Controllerable
{
	const IMAGE_SIZE_WIDTH = 100;
	const ID_IBLOCK_CITY_INFO = 31;
	const ID_IBLOCK_PLUMBING = IBLOCK_ID_CATALOG_SANTEHNIKA;

	protected $errors = array();

	protected $IBLOCK_ID;

	public function __construct($component = null)
	{
		parent::__construct($component);
		$this->errorCollection = new ErrorCollection();
	}

	protected function checkModules()
    {
        if (!Loader::includeModule('iblock') && !Loader::includeModule('catalog'))
            throw new SystemException("Не подключены модули iblock и catalog");
    }

	protected function checkParams()
    {
		$arParams = $this->arParams;
        if (!isset($arParams["PRODUCT_ID"]) || empty($arParams["PRODUCT_ID"]) || (int)$arParams["PRODUCT_ID"] <= 0)
            throw new SystemException("Не передан ID товара");
		$arParams["MEASURE_KVM_ID"] = !empty($arParams["MEASURE_KVM_ID"]) ? $arParams["MEASURE_KVM_ID"] : 20;
		$arParams["AJAX_MODE"] = !empty($arParams["AJAX_MODE"]) ? $arParams["AJAX_MODE"] : "Y";
		$arParams["REGION"] = !empty($arParams["REGION"]) ? $arParams["REGION"] : "Москва";
		$arParams["CITY"] = !empty($arParams["CITY"]) ? $arParams["CITY"] : "Москва";
		$this->arParams = $arParams;
    }

	public function executeComponent()
    {
		try
        {
			$this->checkModules();
			$this->checkParams();
            $this->getResult();
            $this->includeComponentTemplate();
        }
        catch (SystemException $e)
        {
            ShowError($e->getMessage());
        }
	}

	protected function getResult()
    {
		if ($this->errors)
            throw new SystemException(current($this->errors));

		$arResult = [];
		$arParams = $this->arParams;

		$additionalCacheID = false;
        if ($this->startResultCache($arParams['CACHE_TIME'], $additionalCacheID)) {
			$this->getIblockID($arParams["PRODUCT_ID"]);
			$IBLOCK_ID = $this->IBLOCK_ID;
			$PRICE_ID = $this->getPriceID($arParams['REGION'], $arParams['CITY'], $IBLOCK_ID);
			$arSelect = array_merge(
				$arParams["FIELD_CODE"], 
				[
					"IBLOCK_ID",
					"ID",
					"CODE",
					"MEASURE",
					"CATALOG_GROUP_" . $PRICE_ID,
           		]
			);

			$arFilter = array(
                "IBLOCK_ID" => $IBLOCK_ID,
                "IBLOCK_LID" => SITE_ID,
				"ID" => $arParams["PRODUCT_ID"],
                "ACTIVE" => "Y",
            );

			$rsElement = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
			if (!$rsElement) {
                $this->abortResultCache();
            }

			$arResult = $rsElement->Fetch();

			if ($arResult["PREVIEW_PICTURE"] || $arResult["DETAIL_PICTURE"]) {
				$arResult["IMG"] = $this->getPicture($arResult["PREVIEW_PICTURE"] ?: $arResult["DETAIL_PICTURE"], 100, 150);
			}

			$arResult["DETAIL_PAGE_URL"] = '/product/' . $arResult["CODE"] . '/';
			$arResult["PRICE"] = $arResult["CATALOG_PRICE_" . $PRICE_ID];
			$arResult["PRICE_ID"] = $PRICE_ID;

			if (!empty($arParams["SET_STRING"])) {
				$arResult["SET"] = $this->getSet($arParams["SET_STRING"], $PRICE_ID);
			}

			$this->arResult = $arResult;
		}

	}

	protected function getPriceID($region, $city, $IBLOCK_ID)
    {
		if (empty($region) || empty($city) || $IBLOCK_ID <= 0) return 2;

		switch ($IBLOCK_ID) {
			case self::ID_IBLOCK_PLUMBING:
				$propPrice = "PROPERTY_TYPE_PRICE_ID_PLUMBING";
				break;
			default:
				$propPrice = "PROPERTY_TYPE_PRICE_ID";
				break;
		}

		$arSelect = [
			"IBLOCK_ID",
			"ID",
			$propPrice,
		];

		$arFilter = array(
			"IBLOCK_ID" => self::ID_IBLOCK_CITY_INFO,
			"IBLOCK_LID" => SITE_ID,
			"ACTIVE" => "Y",
			"PROPERTY_REGION" => [$region, $city],
		);

		$arResult = CIBlockElement::GetList([], $arFilter, false, [], $arSelect)->Fetch();

		return $arResult[$propPrice . '_VALUE'] ?: 2;
	}

	protected function getSet($setString, $priceID)
    {
		$arIDsQuant = explode(",", $setString);
		$arIDsQuant = array_diff($arIDsQuant, ['']);
		if (empty($arIDsQuant)) return [];
		$arIDs = $arItems = [];
		foreach ($arIDsQuant as $arItem) {
			$arIDQ = explode("|", $arItem);
			if ($arIDQ[0] > 0 && $arIDQ[1] > 0) {
				$arIDs[] = $arIDQ[0];
				$arItems[$arIDQ[0]]["QUANTITY"] = $arIDQ[1];
			}
		}
		if (!empty($arIDs)) {
			$arParams = $this->arParams;
			$IBLOCK_ID = $this->IBLOCK_ID;
			$arSelect = array_merge(
				$arParams["FIELD_CODE"], 
				[
					"IBLOCK_ID",
					"ID",
					"CODE",
					"MEASURE",
					"CATALOG_GROUP_" . $priceID,
           		]
			);

			$arFilter = array(
                "IBLOCK_ID" => $IBLOCK_ID,
                "IBLOCK_LID" => SITE_ID,
				"ID" => $arIDs,
                "ACTIVE" => "Y",
            );
			$rsElement = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
			while ($ar = $rsElement->Fetch()) {
				$arItems[$ar['ID']] = array_merge($arItems[$ar['ID']], $ar);
				$arItems[$ar['ID']]["IMG"] = $this->getPicture($ar["PREVIEW_PICTURE"] ?: $ar["DETAIL_PICTURE"], 75, 75);
				$arItems[$ar['ID']]["PRICE"] = $ar["CATALOG_PRICE_" . $priceID];
			}
		}
		return $arItems;
	}

	protected function getPrice($productID)
    {
		return CPrice::GetBasePrice($productID, 1, 1);
	}

	protected function getIblockID($id)
    {
		if (($IBLOCK_ID = CIBlockElement::GetIBlockByID($id)) > 0)
			$this->IBLOCK_ID = $IBLOCK_ID;
		else
			throw new SystemException("Инфоблок не найден");
	}

	protected function getPicture($id, $width = self::IMAGE_SIZE_WIDTH, $height = 999)
    {
		$arPicture = CFile::ResizeImageGet(
			$id, 
			array(
			   'width' => $width,
			   'height' => $height,
			), 
			BX_RESIZE_IMAGE_PROPORTIONAL,
			[]
		);
		return $arPicture['src'];
	}

	public static function formatPrice($price)
    {
		$price = round($price);
		$formatPrice = number_format($price, 0, ',', ' ');
		return $formatPrice . " ₽";
	}

	public static function transformNumber($number)
	{
		return str_replace(",", ".", $number) * 1;
	}

	
	public static function countDeclension($number, $arWords)
	{
		$countDeclension = new Declension($arWords[0], $arWords[1], $arWords[2]);
		return $countDeclension->get($number);
	}

	public static function getBasketAction($pids)
    {
		if (empty($pids)) return false;

		$arPIDs = [];
		foreach($pids as $pid) {
			$arPidCoof = explode("|", $pid);
			$arPIDs[$arPidCoof[0]] = [
				"ID" => $arPidCoof[0],
				"COEFFICIENT" => $arPidCoof[1],
			];
		}

		$arResult = [];
		$arBasketItems = [];
		$basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
		foreach ($basket as $basketItem) {
			$basketId = $basketItem->getId();
			$productId = $basketItem->getProductId();
			if (isset($arPIDs[$productId])) {
				$arResult["ITEMS"][$basketId] = [
					"BASKET_ID" => $basketId,
					"COEFFICIENT" => $arPIDs[$productId]["COEFFICIENT"],
				];
			}
			$arBasketItems[$basketId] = [
				"QUANTITY" => $basketItem->getQuantity(),
			];
		}

		$fuser = new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId(true));
		$discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, $fuser);
		$discounts->calculate();
		$result = $discounts->getApplyResult(true);
		$arPrices = $result['PRICES']['BASKET'];

		$arResult["ITEM"]["RESULT_PRICE"] = self::getPriceItems($arResult["ITEMS"], $arPrices, $arBasketItems);
		$arResult["ITEM"]["QUANTITY"] = self::getQuantityItem($arBasketItems, $arResult["ITEMS"]);

		$arResult["ALL_SUMM"] = self::getSummBasket($arBasketItems, $arPrices);
		$arResult["ALL_COUNT"] = self::getCountBasket($basket);

		$arResult["BASKET_STRING"] = self::createBasketString($arResult["ITEMS"]);

		return $arResult;
	}

	public static function setQuantityAction($stringIDs, $quantityData)
	{
		$arIds = explode(",", $stringIDs);
		$arIds = array_diff($arIds, ['']);
		$arProducts = [];
		if (count($arIds) > 1) {
			foreach ($arIds as $string) {
				$arRatio = explode("|", $string);
				$arProducts[$arRatio[0]] = $arRatio[1] * $quantityData;
			}
		} else {
			$arProducts[$arIds[0]] = $quantityData;
		}
		$basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
		foreach ($arProducts as $id => $quantity) {
			$item = $basket->getItemById($id);
			$item->setField('QUANTITY', $quantity);
		}
		$refreshStrategy = \Bitrix\Sale\Basket\RefreshFactory::create(\Bitrix\Sale\Basket\RefreshFactory::TYPE_FULL);
		$basket->refresh($refreshStrategy);
		$basket->save();
	}

	public static function getSamplePriceAction($pids)
	{
		if (empty($pids)) return false;

		$arPIDs = [];
		foreach($pids as $pid) {
			$arPidCoof = explode("|", $pid);
			$arPIDs[$arPidCoof[0]] = [
				"ID" => $arPidCoof[0],
				"COEFFICIENT" => $arPidCoof[1],
			];
		}

		$arResult = [];
		$arBasketItems = [];
		$basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
		foreach ($basket as $basketItem) {
			$basketId = $basketItem->getId();
			$productId = $basketItem->getProductId();
			if (isset($arPIDs[$productId])) {
				$arResult["ITEMS"][$basketId] = [
					"BASKET_ID" => $basketId,
					"COEFFICIENT" => $arPIDs[$productId]["COEFFICIENT"],
				];
			}
			$arBasketItems[$basketId] = [
				"QUANTITY" => $basketItem->getQuantity(),
			];
		}

		$fuser = new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId(true));
		$discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, $fuser);
		$discounts->calculate();
		$result = $discounts->getApplyResult(true);
		$arPrices = $result['PRICES']['BASKET'];

		$arResult["ITEM"]["RESULT_PRICE"] = self::getPriceItems($arResult["ITEMS"], $arPrices, $arBasketItems);

		$arResult["BASKET_STRING"] = self::createBasketString($arResult["ITEMS"]);

		return $arResult;
	}

	public static function getSamplesAction()
	{
		$basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
		$arSamples = [];
		foreach ($basket as $basketItem) {
			$basketPropertyCollection = $basketItem->getPropertyCollection(); 
			$arAllProp = $basketPropertyCollection->getPropertyValues();

			foreach ($arAllProp as $arProp) {
				if ($arProp['CODE'] == 'SAMPLE_TILE' && $arProp['VALUE']) {
					$arSamples[] = $basketItem->getProductId();
				}
			}
		}
		return $arSamples;
	}

	public function configureActions()
    {
        return [
            'getBasket' => [
                'prefilters' => [],
            ],
			'setQuantity' => [
                'prefilters' => [],
            ],
			'getSamplePrice' => [
                'prefilters' => [],
            ],
			'getSamples' => [
                'prefilters' => [],
            ],
        ];
    }

	public static function getCountBasket($basket)
    {
		return count($basket->getQuantityList());
	}

	public static function getSummBasket($arItems, $arPrices)
    {
		$summ = 0;
		foreach ($arItems as $idBasket => $item) {
			$summ += $item["QUANTITY"] * $arPrices[$idBasket]["PRICE"];
		}
		return $summ;
	}

	public static function createBasketString($arItems)
    {
		$arResult = [];
		foreach ($arItems as $arItem) {
			$arResult[] = $arItem["BASKET_ID"] . '|' . $arItem["COEFFICIENT"];
		}
		return implode(",", $arResult);
	}

	public static function getPriceItems($arIdsCoef, $arPrices, $arBasketItems){
		$arPrice = [
			"BASE_PRICE" => 0,
			"PRICE" => 0,
			"DISCOUNT" => 0,
		];
		foreach ($arIdsCoef as $arId) {
			$arPrice["BASE_PRICE"] += $arPrices[$arId["BASKET_ID"]]["BASE_PRICE"] * $arBasketItems[$arId["BASKET_ID"]]["QUANTITY"];
			$arPrice["PRICE"] += $arPrices[$arId["BASKET_ID"]]["PRICE"] * $arBasketItems[$arId["BASKET_ID"]]["QUANTITY"];
			$arPrice["DISCOUNT"] += $arPrices[$arId["BASKET_ID"]]["DISCOUNT"] * $arBasketItems[$arId["BASKET_ID"]]["QUANTITY"];
		}

		$arPrice["DISCOUNT_PERCENT"] = self::getDiscountPercent($arPrice["BASE_PRICE"], $arPrice["PRICE"]);

		return $arPrice;
	}

	public static function getQuantityItem($arBasketItems, $arItems){
		$arQuantity = [];
		foreach ($arItems as $bID => $arItem) {
			$quantity = $arBasketItems[$bID]["QUANTITY"] / $arItem["COEFFICIENT"];
			$arQuantity[] = $quantity;
		}
		return min($arQuantity);
	}

	public static function getDiscountPercent($oldPrice, $newPrice){
		return round(100 - $newPrice / ($oldPrice / 100));
	}

}