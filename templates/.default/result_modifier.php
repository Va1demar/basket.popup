<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Handlers\PriceHandler;
use \Settings\MainUtils;

//если плитка измеряется кв.м., то получить цену за единицу и упаковку
$arPriceUnit = [];
if($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"] && !empty($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"])){
    $arPriceUnit["KVM"] = round($arResult["PRICE"]);
    if (!empty($arResult["PROPERTY_KOL_VO_SHT_V_KOROBKE_VALUE"])) {
        $arPriceUnit["UPAK"] = round(CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]) * $arResult["PRICE"]);
        if (!empty($arResult["PROPERTY_KOL_VO_SHT_V_KOROBKE_VALUE"])) {
            $arPriceUnit["SHTUK"] = round($arPriceUnit["UPAK"] / $arResult["PROPERTY_KOL_VO_SHT_V_KOROBKE_VALUE"]);
        }
    }
}
$arResult["PRICE_UNIT"] = $arPriceUnit;

//работа с наборами
if ($arResult["IBLOCK_ID"] == PLITKA_IBLOCK_ID) {
    $arResult["SET"] = [];
}
if (!empty($arResult["SET"])){
    foreach ($arResult["SET"] as &$arSet) {
        $arSet["DETAIL_PAGE_URL"] = '/product/' . $arSet["CODE"] . '/';
    }
    unset($arSet);
}

//id товаров, которые попали в корзину
$arResult["IDS_BASKET"] = [];
if (!empty($arResult["SET"])) {
    foreach ($arResult["SET"] as $arSet) {
        $arResult["IDS_BASKET"][] = $arSet["ID"] . '|' . $arSet["QUANTITY"];
    }
} else {
    $arResult["IDS_BASKET"][] = $arResult["ID"] . '|1';
}

//результат цен
$arResult["RESULT_PRICE"] = [];
if ($arParams["PRICE_CODE"] == 2) {
    if ($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"]) {
        $arResult["RESULT_PRICE"]["NEW_PRICE"] = $arResult["PROPERTY_C_DISCOUNT_PRICE_VALUE"] * CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]);
        $arResult["RESULT_PRICE"]["OLD_PRICE"] = $arResult["PRICE"] * CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]);
    } else {
        $arResult["RESULT_PRICE"]["NEW_PRICE"] = $arResult["PROPERTY_C_DISCOUNT_PRICE_VALUE"];
        $arResult["RESULT_PRICE"]["OLD_PRICE"] = $arResult["PRICE"];
    }
    $arResult["RESULT_PRICE"]["DISCOUNT_PERCENT"] = $arResult["PROPERTY_C_DISCOUNT_RATE_VALUE"];
} else {
    $arResult["RESULT_PRICE"]["DISCOUNT_PERCENT"] = 0;
    if ($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"]) {
        $arResult["RESULT_PRICE"]["OLD_PRICE"] = $arResult["RESULT_PRICE"]["NEW_PRICE"] = $arResult["PRICE"] * CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]);
    } else {
        $arResult["RESULT_PRICE"]["OLD_PRICE"] = $arResult["RESULT_PRICE"]["NEW_PRICE"] = $arResult["PRICE"];
    }
}

//расчёт рассрочки
$resultCredit = \Handlers\PriceHandler::creditTerms($arResult["RESULT_PRICE"]["NEW_PRICE"]);
$arResult["CREDIT_PRICE"] = $resultCredit["PRICE"];
$arResult["CREDIT_PERIOD"] = $resultCredit["PERIOD"];
if ($arParams["REGION"]) {
    if (!in_array($arParams["REGION"], ARRRAY_INSTALLMENTS_REGION)) {
        $arResult["CREDIT_PRICE"] = 0;
    }
}

if (!empty($arResult["PROPERTY_PROIZVODITEL_VALUE"])) {
    if (MainUtils::noShowCredit([$arResult["PROPERTY_PROIZVODITEL_VALUE"]], $arResult["ID"])) {
        $arResult["CREDIT_PRICE"] = 0;
    }
}

//проверка для кнопки "10% на подрезку"
if ($arResult["IBLOCK_ID"] == PLITKA_IBLOCK_ID) {
    $arResult["SHOW_TEN_PERCENT"] = "Y";
    if (isset($arParams["PERCENT_TEN_STOP_KATEGORIYA"])) {
        if (!empty($arParams["PERCENT_TEN_STOP_KATEGORIYA"]))
            $arResult["SHOW_TEN_PERCENT"] = in_array($arResult["PROPERTY_KATEGORIYA_VALUE"], $arParams["PERCENT_TEN_STOP_KATEGORIYA"]) ? "N" : "Y";
    }
} else {
	$arResult["SHOW_TEN_PERCENT"] = "N";
}