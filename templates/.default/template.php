<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$stringTovar = 'товара';
$countElements = 0;
if (!empty($arResult["SET"])) {
    $countElements = count($arResult["SET"]);
    $stringTovar = 'набора';
}
?>
<div class="p-cart__back">
    <div
        class="p-cart<?=$arResult["SHOW_TEN_PERCENT"] == "N" ? " in-row" : ""?>"
        data-products="<?=implode(",", $arResult["IDS_BASKET"])?>"
        data-id-basket=""
        data-iblock="<?=$arResult["IBLOCK_ID"]?>"
    >
        <div class="p-cart__title">Товар успешно добавлен в корзину</div>
        <div class="p-cart-product">
            <?if($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"]):?>
                <div class="p-cart-product__desc-subdesc mobile">
                    Плитка продается коробками.</br>1 коробка =<i><?=$arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]?> м<sup>2</sup></i>
                </div>
            <?endif;?>
            <div class="p-cart-product__info">
                <div class="p-cart-product__image">
                    <a href="<?=$arResult["DETAIL_PAGE_URL"]?>">
                        <img src="<?=$arResult["IMG"]?>" alt="<?=$arResult["NAME"]?>">
                    </a>
                </div>
                <div class="p-cart-product__desc">
                    <div class="p-cart-product__desc-name">
                        <a href="<?=$arResult["DETAIL_PAGE_URL"]?>">
                            <?=$arResult["NAME"]?>
                        </a>
                    </div>
                    <div class="p-cart-product__desc-code">
                        Код <?=$stringTovar?>: <?=$arResult["ID"]?>
                    </div>
                    <?if($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"]):?>
                        <div class="p-cart-product__desc-subdesc">
                            Плитка продается коробками.</br>1 коробка =<i><?=$arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]?> м<sup>2</sup></i>
                        </div>
                    <?endif;?>
                </div>
            </div>
            <div class="p-cart-product__count">
                <div class="p-cart-product__count-input-block">
                    <input type="text" class="p-cart-product__count-input" data-count="<?=$arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"] ? CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]) : 1?>" data-value="0" value="">
                    <span class="p-cart-product__count-sign plus">+</span>
                    <span class="p-cart-product__count-sign minus">-</span>
                    
                </div>
                <?if($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"] && !empty($arResult["PRICE_UNIT"])):?>
                    <div class="p-cart-product__count-measure">
                        <div class="p-cart__measure-name">м<sup>2</sup></div>
                        <div class="p-cart__measure">
                            <div class="p-cart__measure-item act kvm" data-price="<?=$arResult["PRICE_UNIT"]["KVM"]?>" data-unit="м" data-measure="m2" data-count="<?=CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"])?>" data-value="0">м</div>
                            <div class="p-cart__measure-item" data-price="<?=$arResult["PRICE_UNIT"]["SHTUK"]?>" data-unit="шт." data-measure="shtuk" data-count="<?=$arResult["PROPERTY_KOL_VO_SHT_V_KOROBKE_VALUE"]?>" data-value="0">шт.</div>
                            <div class="p-cart__measure-item" data-price="<?=$arResult["PRICE_UNIT"]["UPAK"]?>" data-unit="упак." data-measure="upak" data-count="1" data-value="0">упак.</div>
                        </div>
                    </div>
                <?endif;?>
                <div class="p-cart-product__count-price<?=$arResult["MEASURE"] != $arParams["MEASURE_KVM_ID"]  ? " invisible" : ""?>">
                    <?=CCustomBasketPopup::formatPrice($arResult["PRICE"])?>/<?=$arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"] ? "м<sup>2</sup>" : "шт"?>
                </div>
            </div>
            <?if($arResult["SHOW_TEN_PERCENT"] == "Y"):?>
                <div class="p-cart-product__10percent">
                    <label class="soputka-filter-checkbox__label tile-checked">
                        <span class="soputka-filter-checkbox">
                            <input id="ten-percent" type="checkbox" class="soputka-filter-checkbox__input visually-hidden">
                            <span class="soputka-filter-checkbox__style">
                                <svg class="soputka-filter-checkbox__icon">
                                    <use xlink:href="#svg-checkbox"></use>
                                </svg>
                            </span>
                        </span>
                        <p class="tile-shop-stock__title">+10% на подрезку</p>
                    </label>
                    <div class="tile-note-stock">
                        <span class="tile-note-stock__icon">?</span>
                        <div class="tile-header-smo-popup tile-header-smo-popup_places_tile-note-stock">
                            <div class="tile-header-smo-popup__wrap">
                                <div class="tile-note-stock__text">
                                    <p class="tile-note-stock__text-title">Зачем нужен запас?</p>
                                    <p class="tile-note-stock__text-text">В процессе нарезки и укладки часть плитки повреждается, на такой случай принято закладывать
                                        специальный запас:</p>
                                    <ul class="tile-note-stock__text-list">
                                        <li>• При стандартном способе укладки плитки – 10% от площади.</li>
                                        <li>• При укладе под углом или в помещениях со сложной геометрией – 15% от площади.</li>
                                    </ul>

                                    <p class="tile-note-stock__text-title">Почему стоит купить сразу с запасом?</p>

                                    <p class="tile-note-stock__text-text">В силу особенностей природного материала, тон керамической плитки из
                                        разных партий может незначительно отличаться, что иногда сказывается
                                        на общем впечатлении от ремонта.</p>

                                    <p class="tile-note-stock__text-title">У меня осталась лишняя плитка, что делать?</p>

                                    <p class="tile-note-stock__text-text">Остатки плитки вы всегда можете вернуть нам на склад в течении 2-ух
                                        недель после покупки. Возврат товара принимается кратно коробкам.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <div class="p-cart-product__price-wrap">
                <div class="p-cart-product__price">
                    <div class="p-cart-product__price-old-block" style="display:<?=$arResult["RESULT_PRICE"]["DISCOUNT_PERCENT"] > 0 ? "flex" : "none"?>">
                        <div class="p-cart-product__price-old-price"><?=$arResult["RESULT_PRICE"]["NEW_PRICE"] != $arResult["RESULT_PRICE"]["OLD_PRICE"] ? CCustomBasketPopup::formatPrice($arResult["RESULT_PRICE"]["OLD_PRICE"]) : ""?></div>
                        <div class="p-cart-product__price-old-percent-wrap">
                            <div class="p-cart-product__price-old-percent"><?=$arResult["RESULT_PRICE"]["DISCOUNT_PERCENT"] > 0 ? "-" . $arResult["RESULT_PRICE"]["DISCOUNT_PERCENT"] . "%" : ""?></div>
                        </div>
                    </div>
                    <div class="p-cart-product__price-current"><?=CCustomBasketPopup::formatPrice($arResult["RESULT_PRICE"]["NEW_PRICE"]);?></div>
                    <?if($arResult["CREDIT_PRICE"] > 0):?>
                        <div class="p-cart-product__price-credit">
                            <div class="p-cart-product__price-credit-price"><?=CCustomBasketPopup::formatPrice($arResult["CREDIT_PRICE"]);?></div>
                            <div class="p-cart-product__price-credit-month">х <?=$arResult["CREDIT_PERIOD"]?> мес</div>
                        </div>
                    <?endif;?>
                </div>
            </div>
        </div>
        <?if(!empty($arResult["SET"])):?>
            <?if($countElements > 1):?>
                <div class="p-cart-set">
                    <div class="p-cart-set__title">
                        <div class="p-cart-set__title-name">
                            Состав комплекта <?=$arResult["NAME"]?>:
                        </div>
                        <div class="p-cart-set__title-name mobile">
                            Состав комплекта (<?=$countElements?>)
                        </div>
                        <div class="p-cart-set__title-count">
                            <span><?=$countElements?> <?=CCustomBasketPopup::countDeclension($countElements, ['товар', 'товара', 'товаров'])?></span>
                            <svg class="p-cart-set__title-count-icon">
                                <use xlink:href="#svg-angle"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="p-cart-set__content">
                        <?foreach($arResult["SET"] as $arSet):?>
                            <div class="p-cart-set__item">
                                <a href="<?=$arSet["DETAIL_PAGE_URL"]?>" class="p-cart-set__item-link">
                                    <div class="p-cart-set__item-img"><img src="<?=$arSet["IMG"]?>" alt="<?=$arSet["NAME"]?>"></div>
                                    <div class="p-cart-set__item-name"><?=$arSet["NAME"]?></div>
                                </a>
                                <div class="p-cart-set__item-count"><?=$arSet["QUANTITY"]?> шт.</div>
                                <div class="p-cart-set__item-price">
                                    <?=CCustomBasketPopup::formatPrice($arSet["PRICE"])?>
                                </div>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
            <?endif;?>
        <?endif;?>
        <div class="p-cart-bottom">
            <div class="p-cart-bottom__info-basket"></div>
            <div class="p-cart-bottom__btns">
                <div class="p-cart-btn white" id="continue_buy">
                    <svg class="p-cart__btn-red-icon">
                        <use xlink:href="#svg-arrow"></use>
                    </svg>
                    Продолжить покупки
                </div>
                <a class="p-cart-btn red" href="/cart/">
                    Перейти к оформлению
                </a>
            </div>
        </div>
        <div class="p-cart__cross">
            <svg class="p-cart__cross-icon">
                <use xlink:href="#svg-cross"></use>
            </svg>
            <svg class="p-cart__cross-icon mobile">
                <use xlink:href="#svg-cross-light"></use>
            </svg>
        </div>
    </div>
    <div class="hide">
        <?='<?xml version="1.0" encoding="UTF-8"?>'?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <symbol id="svg-checkbox" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.959 1.118l-6.2 6.34a.137.137 0 01-.198 0l-3.52-3.6a.145.145 0 010-.202l.855-.874a.138.138 0 01.198 0L3.66 5.406 8.906.042a.138.138 0 01.198 0l.855.874a.145.145 0 010 .202z" fill="#C83947"/></symbol>
            <symbol id="svg-cross" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="#282828"/></symbol>
            <symbol id="svg-cross-light" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="#E5E5E5"/></symbol>
            <symbol id="svg-arrow" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.28769 0.5L1.00001 6.5L7.28769 12.5" stroke="#282828"/><path d="M1.35915 6.6283L20.9991 6.6283" stroke="#282828"/></symbol>
            <symbol id="svg-angle" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 7.23975L6 2.00001L0.999998 7.23975" stroke="#909090" stroke-width="2"/></symbol>
        </svg>
    </div>
</div>