<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

//если плитка измеряется кв.м., то получить цену за единицу и упаковку
$arPriceUnit = [];
if($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"] && !empty($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"])){
    $arPriceUnit["KVM"] = round($arResult["PRICE"]);
    if (!empty($arResult["PROPERTY_KOL_VO_SHT_V_KOROBKE_VALUE"])) {
        $arPriceUnit["UPAK"] = round(CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]) * $arResult["PRICE"]);
        if (!empty($arResult["PROPERTY_KOL_VO_SHT_V_KOROBKE_VALUE"])) {
            $arPriceUnit["SHTUK"] = round($arPriceUnit["UPAK"] / $arResult["PROPERTY_KOL_VO_SHT_V_KOROBKE_VALUE"]);
        }
    }
}
$arResult["PRICE_UNIT"] = $arPriceUnit;

//id товаров, которые попали в корзину
$arResult["IDS_BASKET"][] = $arResult["ID"] . '|1';

//результат цен
$arResult["RESULT_PRICE"] = [];
if ($arParams["PRICE_CODE"] == 2) {
    if ($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"]) {
        $arResult["RESULT_PRICE"]["NEW_PRICE"] = $arResult["PROPERTY_C_DISCOUNT_PRICE_VALUE"] * CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]);
        $arResult["RESULT_PRICE"]["OLD_PRICE"] = $arResult["PRICE"] * CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]);
    } else {
        $arResult["RESULT_PRICE"]["NEW_PRICE"] = $arResult["PROPERTY_C_DISCOUNT_PRICE_VALUE"];
        $arResult["RESULT_PRICE"]["OLD_PRICE"] = $arResult["PRICE"];
    }
    $arResult["RESULT_PRICE"]["DISCOUNT_PERCENT"] = $arResult["PROPERTY_C_DISCOUNT_RATE_VALUE"];
} else {
    $arResult["RESULT_PRICE"]["DISCOUNT_PERCENT"] = 0;
    if ($arResult["MEASURE"] == $arParams["MEASURE_KVM_ID"]) {
        $arResult["RESULT_PRICE"]["OLD_PRICE"] = $arResult["RESULT_PRICE"]["NEW_PRICE"] = $arResult["PRICE"] * CCustomBasketPopup::transformNumber($arResult["PROPERTY_KOL_VO_M2_V_KOROBKE_VALUE"]);
    } else {
        $arResult["RESULT_PRICE"]["OLD_PRICE"] = $arResult["RESULT_PRICE"]["NEW_PRICE"] = $arResult["PRICE"];
    }
}