<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$stringTovar = 'товара';
$countElements = 0;
if (!empty($arResult["SET"])) {
    $countElements = count($arResult["SET"]);
    $stringTovar = 'набора';
}
?>
<div class="p-cart__back">
    <div class="p-cart sample" data-samples="<?=implode(",", $arResult["IDS_BASKET"])?>" data-id-basket="">
        <div class="p-cart__title">Образец успешно добавлен в корзину</div>
        <div class="p-cart__description">
            <div>
                Вы можете заказать до 10 образцов плитки за раз. Услуга предоставляется бесплатно, но за образцы необходимо внести залог. Вернуть образцы необходимо в течение 7 дней.
                <a href="/uslugi/dostavka-obraztsov/">Подробнее</a>
            </div>
        </div>

        <div class="p-cart-product">
            <div class="p-cart-product__info">
                <div class="p-cart-product__image">
                    <a href="<?=$arResult["DETAIL_PAGE_URL"]?>">
                        <img src="<?=$arResult["IMG"]?>" alt="<?=$arResult["NAME"]?>">
                    </a>
                </div>
                <div class="p-cart-product__desc">
                    <div class="p-cart-product__desc-name">
                        <a href="<?=$arResult["DETAIL_PAGE_URL"]?>">
                            <?=$arResult["NAME"]?>
                        </a>
                    </div>
                    <div class="p-cart-product__desc-count">
                        Количество: <b>1</b>
                    </div>
                </div>
            </div>
            <div class="p-cart-product__price-wrap sample">
                <div class="p-cart-product__price-title">Сумма залога:</div>
                <div class="p-cart-product__price">
                    <div class="p-cart-product__price-current"></div>
                </div>
            </div>
        </div>
        <div class="p-cart-bottom">
            <div class="p-cart-bottom__info-basket"></div>
            <div class="p-cart-bottom__btns">
                <div class="p-cart-btn white" id="continue_buy">
                    <svg class="p-cart__btn-red-icon">
                        <use xlink:href="#svg-arrow"></use>
                    </svg>
                    Продолжить покупки
                </div>
                <a class="p-cart-btn red" href="/cart/">
                    Перейти к оформлению
                </a>
            </div>
        </div>
        <div class="p-cart__cross">
            <svg class="p-cart__cross-icon">
                <use xlink:href="#svg-cross"></use>
            </svg>
            <svg class="p-cart__cross-icon mobile">
                <use xlink:href="#svg-cross-light"></use>
            </svg>
        </div>
    </div>
    <div class="hide">
        <?='<?xml version="1.0" encoding="UTF-8"?>'?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <symbol id="svg-checkbox" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.959 1.118l-6.2 6.34a.137.137 0 01-.198 0l-3.52-3.6a.145.145 0 010-.202l.855-.874a.138.138 0 01.198 0L3.66 5.406 8.906.042a.138.138 0 01.198 0l.855.874a.145.145 0 010 .202z" fill="#C83947"/></symbol>
            <symbol id="svg-cross" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="#282828"/></symbol>
            <symbol id="svg-cross-light" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z" fill="#E5E5E5"/></symbol>
            <symbol id="svg-arrow" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.28769 0.5L1.00001 6.5L7.28769 12.5" stroke="#282828"/><path d="M1.35915 6.6283L20.9991 6.6283" stroke="#282828"/></symbol>
            <symbol id="svg-angle" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 7.23975L6 2.00001L0.999998 7.23975" stroke="#909090" stroke-width="2"/></symbol>
        </svg>
    </div>
</div>